# Telegram bot template based on pyTelegramBotAPI

Please, enjoy this starter template for Telegram bots based on pyTelegramBotAPI. It includes most common middlewares used by me, MongoDB integration, language picker and internationalization and shows basic encapsulation techniques used by me.

# Installation and local launch

1. Clone this repo: `git clone https://bitbucket.org/bowuse/telebot_template`
2. Launch the [mongo database](https://www.mongodb.com/) locally

And you should be good to go! Feel free to fork and submit pull requests. Thanks!

# Environment variables
- `TOKEN` — Telegram bot token
- `MONGO`— URL of the mongo database
